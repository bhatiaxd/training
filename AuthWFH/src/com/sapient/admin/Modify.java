package com.sapient.admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.model.StudentDAO;

/**
 * Servlet implementation class Modify
 */
@WebServlet("/Modify")
public class Modify extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Modify() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
		out.print("<html><head><title>HI!!</title>"+
				"<body><h1> Enter Student's data</h1>"
				+"<form action='Insert' method='POST'><table>");
		
		out.print("<tr><td> Name:</td><td><input type='text' name='sname'/> </td></tr>"
				+"<tr><td> Username:</td><td><input type='text' name='suname'/> </td></tr>"
				+"<tr><td> Marks:</td><td><input type='number' name='marks'/> </td></tr>"
				+"<tr><td> Password:</td><td><input type='password' name='password'/> </td></tr>"
				+"<tr><td colspan = 2><input type='submit' value='Submit Change'/> </td></tr>"
				+"</form></body></html>");
		
	} catch (Exception e) {
e.printStackTrace();
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Cookie[] c = request.getCookies();
			String s = null;
			for (Cookie cookie : c) {
				if(cookie.getName().equals("userName")){
					s= cookie.getValue();
				}
			}
		new StudentDAO().update(request.getParameter("suname"),request.getParameter("sname"),request.getParameter("marks"),request.getParameter("password"));
		
		RequestDispatcher rd = request.getRequestDispatcher("AdminHome");
		rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
