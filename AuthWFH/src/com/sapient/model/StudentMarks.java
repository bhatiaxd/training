package com.sapient.model;

public class StudentMarks {


	private String userName;
	private Integer marks;
	public StudentMarks(String userName, Integer marks) {
		super();
		this.userName = userName;
		this.marks = marks;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getMarks() {
		return marks;
	}
	public void setMarks(Integer marks) {
		this.marks = marks;
	}
}
