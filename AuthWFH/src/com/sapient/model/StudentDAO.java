package com.sapient.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

	public static Connection c ;
	public static Statement ps; 
	public StudentDAO() throws Exception {
		c = DBConnect.getConnection(); 
        ps = c.createStatement();
	}
	public List<StudentBean> getList() throws SQLException{
		List<StudentBean> studList = new ArrayList<StudentBean>();
		String s = "SELECT S.USERNAME , S.NAME , M.MARKS FROM SDATA AS S , SMARKS AS M WHERE S.USERNAME = M.USERNAME";
		System.out.println(s);
		ResultSet rs = ps.executeQuery(s);
		while (rs.next()) {
			studList.add(new StudentBean(rs.getString(1), rs.getString(2),rs.getInt(3)));
		}
		return studList;
	}
	public int getMarks(String userName) throws Exception {
		
		String s = "SELECT MARKS FROM SMARKS WHERE USERNAME = '"+userName+"'";
		System.out.println(s);
		ResultSet rs = ps.executeQuery(s);
		System.out.println(rs.next()+""+rs.getRow());
		return rs.getInt(1); 
		
	}
	public void insert(String userName, String name, String marks, String password) {
		// TODO Auto-generated method stub
		
	}
	public void update(String userName, String name, String marks, String password) {
		// TODO Auto-generated method stub
		
	}
	public void delete(String s) {
		// TODO Auto-generated method stub
		
	}
}
