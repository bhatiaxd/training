package com.sapient.model;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {
	public static Connection co = null;
	public static Connection getConnection() throws Exception {
	if(co==null) {	
		Class.forName("oracle.jdbc.driver.OracleDriver");
		co = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","sys as sysdba","sys");
	}
	return co;
	}
}
