/**
 * 
 */
package com.sapient.model;

/**
 * @author karbhati1
 *
 */
public class StudentBean {

	private String userName;
	private String name;
	private int marks;
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public StudentBean(String userName, String name,int marks) {
		super();
		this.userName = userName;
		this.name = name;
		this.marks = marks;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
