

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.model.DBConnect;

/**
 * Servlet implementation class Login
 */
@WebServlet(description = "Login Screen", urlPatterns = { "/Login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Login() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			out.print("<html><head><title>HI!!</title></head><body><br><b> Login Window</b>");
			out.print("<form action='Login' method='POST'>");
			out.print("Enter username<input type='text' name='n1'/><br>");
			out.print("Enter password<input type='password' name='n2' /><br>");
			out.print("<p>Please select who yuo are:</p>\r\n" + 
					"  <input type=\"radio\" name=\"cal\" value=1 checked> Admin<br>\r\n" + 
					"  <input type=\"radio\" name=\"cal\" value=2> Student<br>");
			out.print("<input type='submit' value='submit'/><br>");
			out.print("</form></body></html>");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
try {
			
			PrintWriter out = response.getWriter();
			
				String userName =request.getParameter("n1").isEmpty()?null:request.getParameter("n1");
				String password =request.getParameter("n2").isEmpty()?null:request.getParameter("n2");
			if(userName==null || password == null) {

				errorPrint(out);
			}
			else if(request.getParameter("cal").equals("1")) {

				Connection c = DBConnect.getConnection();
				Statement ps =  c.createStatement();
				String s = "SELECT * FROM AUPDATA WHERE USERNAME = '"+userName+"' AND P = '"+password+"'";
				System.out.println(s);
				ResultSet rs = ps.executeQuery(s);
				
				System.out.println(rs.next()+""+rs.getRow());
				if(rs.getRow()>0) {
					RequestDispatcher rd = request.getRequestDispatcher("/AdminHome");
					rd.forward(request, response);
				}else {
					errorPrint(out);
				}
			}else {
				
				Connection c = DBConnect.getConnection();
				Statement ps =  c.createStatement();
				String s = "SELECT * FROM SUPDATA WHERE USERNAME = '"+userName+"' AND P = '"+password+"'";
				System.out.println(s);
				ResultSet rs = ps.executeQuery(s);
				
				System.out.println(rs.next()+""+rs.getRow());
				if(rs.getRow()>0) { 
				RequestDispatcher rd = request.getRequestDispatcher("StudentHome");
				rd.forward(request, response);
				}else {
					errorPrint(out);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void errorPrint(PrintWriter out) {
		out.print("<html><head><title>HI!!</title></head><body><br><b> Login Window</b>");
		
		out.print("<form action='Login' method='POST'>");
		out.print("Enter username<input type='text' name='n1'/><br>");
		out.print("Enter password<input type='password' name='n2' /><br>");
		out.print("<p>Please select who yuo are:</p>\r\n" + 
				"  <input type=\"radio\" name=\"cal\" value=1 checked> Admin<br>\r\n" + 
				"  <input type=\"radio\" name=\"cal\" value=2> Student<br>");
		out.print("<input type='submit' value='submit'/><br>");
		out.print("<script>alert(\"Enter Everything correctly!!\");</script>");
		out.print("</form></body></html>");
	}


}
