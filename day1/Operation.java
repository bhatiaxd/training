import java.util.*;

class Operation {
	public static void main(String... args){
		Scanner sc = new Scanner(System.in);
		Map<Integer,Operator> m = new HashMap();
		m.put(1,new Add());
		m.put(2,new Sub());
		m.put(3,new Mul());
		m.put(4,new Div());


		int a = sc.nextInt();

		int b = sc.nextInt();


		int choice = sc.nextInt();

		try{
			m.get(choice).operation(a,b);
		}
		catch(Exception e){
			System.out.println("invalid choice");
		}
	}
}