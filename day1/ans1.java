interface A{
	
	int cal(int a,int b);
}

class Demo{
	
	public static void main(String... args){
		A[] a = new A[4];
		a[0] = (x,y) -> (x+y);
		a[1] = (x,y) -> (x-y);
		a[2] = (x,y) -> (x*y);
		a[3] = (x,y) -> (x/y);

		System.out.println(a[Integer.parseInt(args[2])-1].cal(Integer.parseInt(args[0]),Integer.parseInt(args[1])))
	}


}