package com.sapient.week1.day3;

import java.util.Scanner;

public class Display {

	public Scanner sc;
	
	public Display() {
		sc =  new Scanner(System.in);
	}
	
	public int input() {
		String displayInput = "Enter number: ";
		System.out.println(displayInput);
		int num = sc.nextInt();
		return num;
	}
	
	public void outputNumber(int num) {
		String displayOutput = "Number is: " + num;
		System.out.println(displayOutput);
	}
	
	public void outputResult(int num) {
		String displayOutput = "Sum is: " + num;
		System.out.println(displayOutput);
	}
}
