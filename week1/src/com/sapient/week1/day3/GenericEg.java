package com.sapient.week1.day3;

public class GenericEg {

	public static <E> void display(E... s) {
		for (int i = 0; i < s.length; i++) {
			System.out.print(s[i]+"\t");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Integer arr[] = {1,2,3,4};
		String  ara[] = {"ffff","f","13","dddddd"};
		display(arr);
		display(ara);
	}
}
