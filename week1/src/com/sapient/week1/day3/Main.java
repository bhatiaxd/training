package com.sapient.week1.day3;

public class Main {

	public static void main(String[] args) {
		
		Display d = new Display();
		Number num1 = new Number(d.input());
		Number num2 = new Number(d.input());
		Compute c = new Compute();
		Number num3 = c.add(num1,num2);
		d.outputResult(num3.getNum());
		num1 = null;
		num2 = null;
		num3 = null;
		d = null;
		c = null;
		
	}

}
