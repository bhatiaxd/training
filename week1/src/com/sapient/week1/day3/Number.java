package com.sapient.week1.day3;

public class Number {
	
	/*
	 * num is our data 
	 */
	private int num;
	
	/*
	 * Constructor for the class
	 */
	public Number(int num) {
		this.num = num;
	}
	
	/*
	 * getter method for num
	 */
	public int getNum() {
		return num;
	}
	
	/*
	 * setter method for num
	 */
	public void setNum(int num) {
		this.num = num;
	}
}
