package com.sapient.week1.day3;

import java.util.Arrays;

public class ArrayDistinct {

	public static void main(String[] args) {
		int arr[] = new int[] {1,2,3,4,4,5,6,4,54,56,4,5,5};
		arr=Arrays.stream(arr).distinct().toArray();
	    Arrays.stream(arr).forEach(System.out::println);
}
}
