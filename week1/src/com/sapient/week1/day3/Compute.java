package com.sapient.week1.day3;

public class Compute {

	/*
	 * method to add two Number object
	 */
	public Number add(Number operand1, Number operand2) {
		Number result = new Number(operand1.getNum() + operand2.getNum());
		return result;
	}
}
