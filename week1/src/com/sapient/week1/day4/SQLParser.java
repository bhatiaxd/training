package com.sapient.week1.day4;

import java.util.ArrayList;
import java.util.Scanner;

public class SQLParser {
	
	public static ArrayList<PersonBean> a = new ArrayList<>();
	
	public static void main(String[] args) {

		a.add(new PersonBean("Krwr",21,"delhi",85000));
		a.add(new PersonBean("Kre",44,"delhi",85220));
		a.add(new PersonBean("wrK",24,"delhi",65020));
		a.add(new PersonBean("ewrK",51,"delhi",85020));
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Type no of attributes ");
		int n = sc.nextInt();
		sc.nextLine();
		String s[] = new String[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Attribute"+i+":");
			s[i]= sc.nextLine().toLowerCase();
		}
		print(s);
	}
	public static void print(String[] s) {
		String result ="";
		for (String string : s) {
			if(string.equals("name")) {
				result += "Name" + "\t";
			}else if(string.equals("age")) {
				result += "Age" + "\t";
			}else if(string.equals("city")) {
				result += "City" + "\t";
			}else if(string.equals("salary")) {
				result += "Salary" + "\t";
			}
		}
		result +="\n";
		for (PersonBean p : a) {
			for (String string : s) {
					if(string.equals("name")) {
						result += p.getName() + "\t";
					}else if(string.equals("age")) {
						result += p.getAge() + "\t";
					}else if(string.equals("city")) {
						result += p.getCity()+ "\t";
					}else if(string.equals("salary")) {
						result += p.getSalary() + "\t";
					}
				}
			result += "\n";
			}
			System.out.println(result);
		}
	
}
