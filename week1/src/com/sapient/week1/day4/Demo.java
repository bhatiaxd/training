package com.sapient.week1.day4;


public class Demo {
	
	public static void main(String[] args) {
		EmployeeData ed = new EmployeeData();
		ed.insertData(new Employee("Karan",21,22));
		ed.insertData(new Employee("sss",211,122));
		ed.insertData(new Employee("ssxs",11,12));
		for (Employee emp : ed.getList(0)) {
			System.out.println(emp);
		}
	}
}
