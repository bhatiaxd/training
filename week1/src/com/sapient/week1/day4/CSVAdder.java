package com.sapient.week1.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class CSVAdder {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(
				new FileReader("C:\\Users\\karbhati1\\Desktop\\training\\week1\\src\\com\\sapient\\week1\\day4\\k"));
		BufferedWriter fw = new BufferedWriter(new FileWriter(
				"C:\\Users\\karbhati1\\Desktop\\training\\week1\\src\\com\\sapient\\week1\\day4\\result"));
		String s = br.readLine();
		while (s != null) {
			int sum = 0;
			String[] arr = s.split(",", 100);
			for (String string : arr) {
				sum += Integer.parseInt(string);
			}
			fw.write(s + "=" + sum+"\n");
			s = br.readLine();
		}
		br.close();
		fw.close();
	}
}
