package com.sapient.week1.day4;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeData {

	Map<Integer,Employee> map = new HashMap<>();
	
	public void insertData(Employee emp) {
		map.put(emp.getEid(), emp);
	}
	
	public Employee getData(Integer key) {
		return map.get(key);
	}
	
	public List<Employee> getList(int i){
		List<Employee> l1 = map.values().stream().collect(Collectors.toList());
		Collections.sort(l1);
		return l1;
	}
}
