package com.sapient.week1.day4;

public class Employee implements Comparable<Employee>{

	private int eid;
	private String name;
	private int age;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int compareTo(Employee o) {
		return this.name.compareTo(o.name);
	}
	@Override
	public String toString() {
		return eid + "\t" + name + "\t" + age;
	}
	public Employee(String name, int eid,  int age) {
		this.eid = eid;
		this.name = name;
		this.age = age;
	}
}
