package com.sapient.week1.day2;

import java.util.Arrays;
import java.util.Scanner;

public class IntegerArray {
	public int[] a;

	public void read(Scanner sc) {
		for (int i = 0; i < a.length; i++) {
			a[i]=sc.nextInt();
		}
	}

	public void display() {
		System.out.println("Array is");
		Arrays.stream(a).forEach(System.out::println); 
	}

	public void average() {
		System.out.println(Arrays.stream(a).average().getAsDouble()); 
	}

	public IntegerArray() {
		this.a = new int[10];
	}

	public IntegerArray(IntegerArray a) {
		this.a = a.a;
	}

	public IntegerArray(int size) {
		this.a = new int[size];
	}
	
	public void adoptArray(int[] b) {
			a = b;
	}
	
	public void sort() {
		Arrays.sort(this.a);
	}
	
	public static void main(String[] args) {
		System.out.println("Enter size ");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		IntegerArray t1 = new IntegerArray(size);
		t1.read(sc);
		t1.display();
		
		System.out.println("Enter size ");
		size = sc.nextInt();
		IntegerArray t2 = new IntegerArray(size);
		t2.read(sc);
		t2.display();
		
		System.out.println("adopting");
		t1.adoptArray(t2.a);
		t1.display();
		
		System.out.println("sort");
		t1.sort();
		t1.display();
		
		System.out.println("avg");
		t1.average();
		
	}
	
}
