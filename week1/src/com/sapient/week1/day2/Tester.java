package com.sapient.week1.day2;
import static org.junit.Assert.assertEquals;

import org.junit.*;
public class Tester {
	
	PrimeNotPrime p;
	@Before
	public void init() {
		p = new PrimeNotPrime();
	}
	@Test
	public void validator1() {
		assertEquals(1, p.check(2));
	}	

	@Test
	public void validator2() {
		assertEquals(0, p.check(45));
	}	

	@Test
	public void validator3() {
		assertEquals(0, p.check(20));
	}	
	
	@Test
	public void validator4() {
		assertEquals(1, p.check(3));
	}
}
