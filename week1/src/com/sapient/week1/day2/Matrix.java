package com.sapient.week1.day2;

import java.util.Scanner;

public class Matrix {

	public int[][] a;
	public int r, c;

	public Matrix() {
		this(3, 3);
	}

	public Matrix(int i, int j) {
		this.a = new int[i][];
		for (int k = 0; k < i; k++) {
			a[k] = new int[i];
		}
		r = i;
		c = j;
	}

	public void read(Scanner sc) {
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				a[i][j] = sc.nextInt();
			}
		}
	}

	public void display() {
		System.out.println("Matrix is");
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(a[i][j]+"\t");
			}
			System.out.println();
		}
	}

	public void adoptArray(int[][] b) {
		a = b;
	}

	public Matrix add(Matrix b) {
		Matrix x = new Matrix(a.length,a[0].length);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x.a[i][j]=a[i][j]+b.a[i][j];
			}
		}
		return x;
	}
	public Matrix sub(Matrix b) {
		Matrix x = new Matrix(a.length,a[0].length);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x.a[i][j]=a[i][j]-b.a[i][j];
			}
		}
		return x;
	}
	public static void main(String[] args) {
		System.out.println("Enter size ");
		Scanner sc = new Scanner(System.in);
		int size1 = sc.nextInt();
		int size2 = sc.nextInt();
		Matrix t1 = new Matrix(size1,size2);
		t1.read(sc);
		t1.display();

	}
}
