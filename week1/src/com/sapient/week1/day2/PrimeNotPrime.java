package com.sapient.week1.day2;

public class PrimeNotPrime {

	public int check(int number){

		if(number==2)
			return 1;

		if(number%2==0)
			return 0;
		
		for(int i=3;i<Math.sqrt(number);i+=2) {
			if(number%i==0)
				return 0;
		}
		return 1;
	}
}
