package com.sapient.week1.day5;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	public List<PlayerBean> getPlayers() throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,firstname,lastname,jersey from players");
		ResultSet rs = ps.executeQuery();
				System.out.println(rs.getFetchSize());
List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public List<PlayerBean> getAnyPlayer(int id) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,firstname,lastname,jersey from players where id =? ");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public int insertPlayer(PlayerBean ob) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("insert into players (id,firstname,lastname,jersey) values (?,?,?,?) ");
		ps.setInt(1, ob.getId());ps.setString(2, ob.getFname());ps.setString(3, ob.getLname());ps.setInt(4, ob.getJerseyno());
		int rs = ps.executeUpdate();
		return rs;
	}
	
	public int updatePlayer(PlayerBean ob) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("update players set firstname =?,lastname=?,jersey=? where id= ? ");
		ps.setInt(4, ob.getId());ps.setString(1, ob.getFname());ps.setString(2, ob.getLname());ps.setInt(3, ob.getJerseyno());
		
		int rs = ps.executeUpdate();
		return rs;
	}
}
