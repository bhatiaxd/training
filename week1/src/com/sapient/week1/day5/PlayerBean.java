package com.sapient.week1.day5;

public class PlayerBean {
	private int id;
	private String fname;
	private String lname;
	private int jerseyno;
	
	@Override
	public String toString() {
		return "PlayerBean [id=" + id + ", fname=" + fname + ", lname=" + lname + ", jerseyno=" + jerseyno + "]";
	}
	public PlayerBean(int id, String fname, String lname, int jerseyno) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.jerseyno = jerseyno;
	}
	public PlayerBean() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public int getJerseyno() {
		return jerseyno;
	}
	public void setJerseyno(int jerseyno) {
		this.jerseyno = jerseyno;
	}
	
}
