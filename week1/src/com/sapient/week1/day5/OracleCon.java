package com.sapient.week1.day5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class OracleCon {
	public static void main(String[] args) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","sudarshan");
			Statement stmt = con.createStatement();
			//ResultSet rs = stmt.executeQuery("INSERT INTO playerinfo (id,fname,lname,jerseyno) values(02,'MOHIT','SINGH',11)");
			ResultSet rs = stmt.executeQuery("select * from playerinfo");
			while(rs.next()) {
				System.out.println(rs.getInt(1)+ "  " + rs.getString(2) + "  " + rs.getString(3) + "  " + rs.getInt(4)  );
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
