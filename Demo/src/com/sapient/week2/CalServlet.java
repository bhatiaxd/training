package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CalServlet
 */
public class CalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		int n1 = Integer.parseInt(request.getParameter("n1"));
		int n2 = Integer.parseInt(request.getParameter("n2"));
		int n4 = Integer.parseInt(request.getParameter("cal"));
		int n3;
		switch (n4) {
		case 1:n3=n1+n2;
			break;
		case 2:n3=n1-n2;
			
			break;
		case 3:n3=n1*n2;
				
			break;

		default:n3=n1/n2;
			break;
		}

		RequestDispatcher rd=request.getRequestDispatcher("/calculator.jsp");  
		rd.include(request, response); 

		request.getSession().setAttribute("n1", n1);
		System.out.println(request.getAttribute("n1"));
		request.setAttribute("n2", n2);
		request.setAttribute("n3", n3);
		request.setAttribute("cal", n4);
		rd.forward(request,response);
	}

}
