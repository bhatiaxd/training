package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Form
 */
public class Form extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Form() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print("<html><body>Hi!! <br><b> ADD 2 Number</b>");
		out.print("<form action='Form' method='POST'>");
		out.print("Enter no. 1<input type='text' name='n1' value='0'/><br>");
		out.print("Enter no. 2<input type='text' name='n2' value='0'/><br>");
		out.print("Result <input type='text' name='n3' value='0'/><br>");

		out.print("<input type='submit' value='submit'/><br>");
		out.print("</form></body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		int n1 = Integer.parseInt(request.getParameter("n1"));
		int n2 = Integer.parseInt(request.getParameter("n2"));
		int n3 = n1+n2;
		out.print("<html><body>Hi!! <br><b> ADD 2 Number</b>");
		out.print("<form action='Form' method='POST'>");
		out.print("Enter no. 1<input type='text' name='n1' value='"+n1+"'/><br>");
		out.print("Enter no. 2<input type='text' name='n2' value='"+n2+"'/><br>");
		out.print("Result <input type='text' name='n3' value='"+n3+"'/><br>");

		out.print("<input type='submit' value='submit'/><br>");
		out.print("</form></body></html>");
	}

}
