package com.sapient.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.model.StudentBean;
import com.sapient.model.StudentDAO;

/**
 * Servlet implementation class AdminHome
 */
@WebServlet(description = "Home screen of Admin", urlPatterns = { "/AdminHome" })
public class AdminHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminHome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		StudentDAO sdao;
		try {
			//sdao = new StudentDAO();
			//List<StudentBean> sList = sdao.getList();
			List<StudentBean> sList = new ArrayList<>();
			sList.addAll(Arrays.asList(new StudentBean("kra", "kara1n", 12),
					new StudentBean("kr12a", "ka111ran", 121),new StudentBean("kra", "ka112ra1n", 112)));
			
		out.print("<html><head><title>HI!!</title>"+
				"<script>function setCookie(cvalue) {document.cookie = \"userName =\" + cvalue + \";\" ;" + 
				"}</script></head><body><form action=\"Logout\" method=\"GET\"><input type='submit' value='Logout'/><br><b> List of your students </b>"+
				"<table><tr><td>Roll</td><td>Name</td><td>Marks</td><td>Update</td><td>Delete</td>");
		
			for (StudentBean studentBean : sList) {
				out.print("<tr><td>"+studentBean.getUserName()+"</td><td>"
						+studentBean.getName()+"</td><td>"
						+studentBean.getMarks()+"</td><td>"
						+"<form action=\"Modify\" method='POST'><button onclick=\"setCookie('"+studentBean.getUserName()+"')\"  id=\"btn\">Modify</button></form></td>"
						+"<td><form action=\"Delete\" method='POST'><button onclick=\"setCookie('"+studentBean.getUserName()+"')\"  id=\"btn\">Delete</button></form></td>"
						+"</tr>");
			}
			out.print("</table><br><br><form action=\"Insert\" method=\"GET\"><input type='submit' value='New Student'/></form></body></html>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
