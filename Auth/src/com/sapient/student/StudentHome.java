package com.sapient.student;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.model.StudentMarksDAO;

/**
 * Servlet implementation class StudentHome
 */
@WebServlet(description = "Home Screen of Student", urlPatterns = { "/StudentHome" })
public class StudentHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentHome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		out.print("<html><head><title>HI!!</title></head><body><br><b> Home</b>");
		out.print("<form action='StudentHome' method='POST'>");
		out.print("Enter Registration No.<input type='text' name='n1'/><br><br>");
		out.print("<input type='submit' value='Result of your Friend'/><br>");
		out.print("</form></body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Integer marks =-1;
		String userName =request.getParameter("n1").isEmpty()?null:request.getParameter("n1");
		
		try {
			marks = new StudentMarksDAO().getMarks(userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(userName==null || marks==-1) {
			out.print("<html><head><title>HI!!</title></head><body><br><b> Home</b>");
			out.print("<form action='StudentHome' method='POST'>");
			out.print("Enter Registration No.<input type='text' name='n1'/><br><br>");
			out.print("<input type='submit' value='Result of your Friend'/><br>");
			
			out.print("<script>alert(\"Enter Everything correctly!!\");</script>");
			out.print("</form></body></html>");
		}
		else {
			out.print("<html><head><title>HI!!</title></head><body><br><b> Home</b><br>");
			out.print("Registration No.:"+userName+"<br><br>");
			out.print("Marks:"+marks+"<br><br>");
			out.print("<form action='StudentHome' method='GET'>");
			out.print("<input type='submit' value='Result of your Friend'/><br>");
			out.print("</form></body></html>");

		}
	}

}
