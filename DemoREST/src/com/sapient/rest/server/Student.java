/**
 * 
 */
package com.sapient.rest.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.*;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * @author karbhati1
 *
 */
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class Student {
	
	@GET  
	@Produces(MediaType.TEXT_PLAIN)  	  
	public String getString1(){
		return "Karan is genious";
	}
	@GET  
	  @Path("/{param}")  
	  public Response getMsg(@PathParam("param") String msg) {  
	      String output = "Jersey say : " + msg;  
	      return Response.status(200).entity(output).build();  
	  }
	@GET
	  
	  @Path("sud/{param}")  
	  public Response getDetail(@PathParam("param") int msg) {  
		String output="";
		try {
			Connection co=DBConnection.getConnection();
			PreparedStatement ps=co.prepareStatement("select * from student where id=?");
			ps.setInt(1, msg);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				output=rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4); 
			}else{
				output="Invalid id";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	     return Response.status(200).entity(output).build();  
	  }
	@GET  
	  @Path("{year}/{month}/{day}")  
	  public Response getDate(  
	          @PathParam("year") int year,  
	          @PathParam("month") int month,   
	          @PathParam("day") int day) {  
	 
	     String date = year + "/" + month + "/" + day;  
	 
	     return Response.status(200)  
	      .entity("getDate is called, year/month/day : " + date)  
	      .build();  
	  } 
	
	@POST
	  @Path("/add1")  
	  public Response addStudent(  
	      @FormParam("id") String id,  
	      @FormParam("fname") String fname,  
	      @FormParam("lname") String lname,
	      @FormParam("DOB") String dob,
	      @FormParam("DNo") String dno,
	      @FormParam("St1") String st1,
	      @FormParam("St2") String st2,
	      @FormParam("gender") String gender
	      ) {  
	 String msg=insert(id,fname,lname,dob,dno,st1,st2,gender);
			 
			 return Response.status(200)  
	          .entity(msg)  
	          .build();  
	  } 
	@GET  
	  @Path("{id}/{fname}/{lname}/{dob}/{dno}/{st1}/{st2}/{gn}")  
	  public Response addStudent1(  
	          @PathParam("id") String id,  
	          @PathParam("fname") String fname,  
	          @PathParam("lname") String lname,
	          @PathParam("dob") String dob,
	          @PathParam("dno") String dno,
	          @PathParam("st1") String st1,
	          @PathParam("st2") String st2,
	          @PathParam("gn") String gn
	        
			  ) {  
	 
	     String msg = insert(id,fname,lname,dob,dno,st1,st2,gn);  
	 
	     return Response.status(200)  
	      .entity(msg)  
	      .build();  
	  } 
	
	public String insert(String ...a1)
	{
		try{
			Connection co=DBConnection.getConnection();
			PreparedStatement ps=co.prepareStatement("Insert into student values(?,?,?,?,?,?,?,?)");
			for(int i=1;i<=a1.length;i++)
			{
				ps.setString(i, a1[i-1]);
			}
			ps.executeQuery();
			return "Inserted";
		}
		catch (Exception e) {
			// TODO: handle exception
			return e.getMessage();
		}
	}
	
	
	
	private static final String FILE_PATH1 = "\\download.png";  
	  @GET  
	  @Path("/image")  
	  @Produces("image/png")  
	  public Response getFile1() {  
	      File file = new File(FILE_PATH1);  
	      ResponseBuilder response = Response.ok((Object) file);  
	      response.header("Content-Disposition","attachment; filename=\"download.png");  
	      return response.build();  
	 
	  }  
	
	  private static final String FILE_PATH = "\\info.txt";  
	  @GET  
	  @Path("/txt")  
	  @Produces("text/plain")  
	  public Response getFile() {  
	      File file = new File(FILE_PATH);  
	 
	      ResponseBuilder response = Response.ok((Object) file);  
	      response.header("Content-Disposition","attachment; filename=\"info.txt");  
	      return response.build();  
	 
	  }
	  
	  @GET  
	  @Path("/json/{id}")
	  @Produces(MediaType.APPLICATION_JSON)  
	  public Response sayXMLHello( @PathParam("id") String id) {  
		  
		  String output="";
			try {
				Connection co=DBConnection.getConnection();
				PreparedStatement ps=co.prepareStatement("select * from student where id=?");
				ps.setString(1, id);
				ResultSet rs=ps.executeQuery();
				if(rs.next()){
					output="{ \"fname\" : \""+rs.getString(2)+"\", \"lname\" : \""+rs.getString(3)
					+"\", \"dob\" : \""+rs.getString(4)+"\"}"; 
				}else{
					output="Invalid id";
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		     return Response.status(200).entity(output).build();    
	  }  
	
}
