package com.sapient.rest.server;

import java.sql.*;

public class DBConnection {
static Connection con = null;
	
	public static Connection getConnection() throws Exception {
		try {
			if(con==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@10.151.62.234:1521:xe","sys as sysdba","sys"); 
			con.setAutoCommit(true);
		} 
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
		
	}

}
