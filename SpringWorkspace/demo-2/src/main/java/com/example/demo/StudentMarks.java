package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "marks")
public class StudentMarks {

	@Id
	private String id;
	
	@ManyToOne
    @JoinColumn
	private StudentDetails reg;
	@Column
	private int testid;
	@Column
	private int marks1;
	@Column
	private int marks2;
	@Column
	private int marks3;
	public StudentMarks() {
	}
	public StudentMarks(String id, StudentDetails reg, int testid, int marks1, int marks2, int marks3) {
		super();
		this.id = id;
		this.reg = reg;
		this.testid = testid;
		this.marks1 = marks1;
		this.marks2 = marks2;
		this.marks3 = marks3;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public StudentDetails getReg() {
		return reg;
	}
	public void setReg(StudentDetails reg) {
		this.reg = reg;
	}
	public int getTestid() {
		return testid;
	}
	public void setTestid(int testid) {
		this.testid = testid;
	}
	public int getMarks1() {
		return marks1;
	}
	public void setMarks1(int marks1) {
		this.marks1 = marks1;
	}
	public int getMarks2() {
		return marks2;
	}
	public void setMarks2(int marks2) {
		this.marks2 = marks2;
	}
	public int getMarks3() {
		return marks3;
	}
	public void setMarks3(int marks3) {
		this.marks3 = marks3;
	}
}
