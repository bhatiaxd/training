/**
 * 
 */
package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * @author karbhati1
 *
 */
@Service
public interface StudentDetailsDAO extends CrudRepository<StudentDetails, String> {

}
