package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "student")
public class StudentDetails {
	
	@Id
	private int reg;
	
	@Column
	private String fname;

	@Column
	private String lname;

	@Column
	private String dob;

	@Column
	private String cityCode;


	@Transient
	@OneToMany( mappedBy = "student")
	Set<StudentMarks> s = new HashSet<>();
	
	public StudentDetails() {
		super();
	}
	public Set<StudentMarks> getS() {
		return s;
	}
	
	public void setS(Set<StudentMarks> s) {
		this.s = s;
	}

	public StudentDetails(int id, String fname, String lname, String dob, String cityCode) {
		super();
		this.reg = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.cityCode = cityCode;
	}



	public int getReg() {
		return reg;
	}

	public void setReg(int reg) {
		this.reg = reg;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	
}
