/**
 * 
 */
package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author karbhati1
 *
 */
@RestController
public class Controller {

	@Autowired
	StudentDetailsDAO sd;
	@Autowired 
	StudentMarksDAO sm;
	
	@PostMapping("/adds")
	public String storSD(@RequestBody StudentDetails s) {
		sd.save(s);
		return "Added";
	}
	
	@PostMapping("/addm")
	public String storSM(@RequestBody StudentMarks s) {
		sm.save(s);
		return "Added";
	}
	
	@GetMapping("/displayS")
	public List<StudentDetails> diaplayS(){
		return (List<StudentDetails>) sd.findAll();
	}
	
	@GetMapping("/displayM")
	public List<StudentMarks> diaplayM(){
		return (List<StudentMarks>) sm.findAll();
	}
	
}
