package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	StudentDAO ob;

	@GetMapping(value = "/hello")
	public String display() {
		return "<h1>Karan is saying u r stupid</h1>";
	}

	@GetMapping(value =  "/hellostudent")
	public Student displayStudent() {
		return new Student("Karan", 22, "Delhi");
	}

	@GetMapping(value = "/hellostudentlist")
	public List<Student> getStudents() {
		return ob.getStudent();
	}

	@GetMapping( value = "/hellostudentlist/{name}")
	public List<Student> getStudentsByName(@PathVariable String name) {
		return ob.getStudentByName(name);
	}

	@PostMapping(value = "/insert")
	public String insert(@RequestBody Student s) {
		return ob.insert(s);
	}

	@DeleteMapping(value = "/deletes")
	public String delete(@RequestParam("name") String s) {
		return ob.delete(s);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(@RequestBody Student s) {
		return ob.update(s);
	}
	
}
