package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {

	@Autowired
	public StudentList l1;
	
	public List<Student> getStudent(){
		return l1.getList();
	}
	
	public List<Student> getStudentByName(String name){
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	public String insert(Student s) {
		l1.getList().add(s);
		return  "INSERT DONE";
	}
	
	public String delete(String name) {
		l1.getList().removeAll(l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList()));
		return  "DELETE DONE";
	}
	
	public String update(Student s) {
		l1.getList().removeAll(l1.getList().stream().filter(e->e.getName().equals(s.getName())).collect(Collectors.toList()));
		l1.getList().add(s);
		return  "UPDATE DONE";
	}
}
