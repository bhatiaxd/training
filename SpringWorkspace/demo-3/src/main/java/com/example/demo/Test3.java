package com.example.demo;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
class Read {
		public static Scanner sc = new Scanner(System.in);
	}
public class Test3 {

	

	static final String URL_CREATE_EMPLOYEE = "http://10.151.62.234:8012/";

	public static void main(String[] args) throws RestClientException, URISyntaxException {

		Student newEmployee = new Student();
		System.out.println("enter age, name , city");

		newEmployee.setAge(Integer.parseInt(Read.sc.next()));
		newEmployee.setName(Read.sc.next());
		newEmployee.setCity(Read.sc.next());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);

		RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<Student> requestBody = new HttpEntity<>(newEmployee, headers);

		// Send request with POST method.
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE+"insert", requestBody, String.class);

		if (e != null) {

			System.out.println("Employee created: ");
		} else {
			System.out.println("Something error!");
		}
		System.out.println(restTemplate.getForObject(URL_CREATE_EMPLOYEE+"hellostudentlist", String.class));


		HttpHeaders headersUpdate = new HttpHeaders();
		headersUpdate.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headersUpdate.setContentType(MediaType.APPLICATION_JSON);

		newEmployee.setAge(23);
		// Data attached to the request.
		HttpEntity<Student> requestBody1 = new HttpEntity<>(newEmployee, headers);

		// Send request with POST method.
		restTemplate.put(new URI(URL_CREATE_EMPLOYEE+"update"), requestBody1);
		System.out.println(restTemplate.getForObject(URL_CREATE_EMPLOYEE+"hellostudentlist", String.class));

		restTemplate.exchange(URL_CREATE_EMPLOYEE+"deletes?name=\""+newEmployee.getName()+"\"",HttpMethod.DELETE,requestBody1,String.class).getBody();
		System.out.println(restTemplate.getForObject(URL_CREATE_EMPLOYEE+"hellostudentlist", String.class));

		

	}
}
