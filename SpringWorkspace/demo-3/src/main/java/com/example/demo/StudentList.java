package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
	
	private List<Student> list;

	public StudentList() {
		super();
		list = new ArrayList<>();
		list.add(new Student("karan", 22, "Delhi"));
		list.add(new Student("samson", 23, "Pune"));
		list.add(new Student("ishbir", 22, "Delhi"));
		list.add(new Student("snigdha", 21, "Indore"));
	}

	public StudentList(List<Student> list) {
		super();
		this.list = list;
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}

}
