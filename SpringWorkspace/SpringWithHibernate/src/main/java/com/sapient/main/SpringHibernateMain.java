package com.sapient.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sapient.bean.Employee;
import com.sapient.dao.EmployeeDAO;

public class SpringHibernateMain {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		EmployeeDAO eDAO;
		eDAO = (EmployeeDAO) context.getBean("eDAO");
		Logger logger = LoggerFactory.getLogger(SpringHibernateMain.class);
		
		Employee emp ;
		emp = new  Employee();
		emp.setAge(54);
		emp.setSalary(27064);
		logger.debug("emp init");
		eDAO.save(emp);
		eDAO.list().forEach(e -> logger.debug(e.toString()));	
		
		//close resources
		((AbstractApplicationContext) context).close();	
	}

}
