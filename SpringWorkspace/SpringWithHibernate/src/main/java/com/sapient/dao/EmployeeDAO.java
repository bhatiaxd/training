package com.sapient.dao;

import java.util.List;

import com.sapient.bean.Employee;

public interface EmployeeDAO {

	public void save(Employee e);

	public List<Employee> list();
}
