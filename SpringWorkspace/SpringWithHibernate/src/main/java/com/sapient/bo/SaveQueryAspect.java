package com.sapient.bo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.sapient.bean.Employee;

@Aspect
public class SaveQueryAspect {

	
	@Before("execution(void com.sapient.dao.EmployeeDAOImpl.save(*))")
	public void check(JoinPoint jpoint) {
		for (Object obj : jpoint.getArgs()) {
			Employee e =  (Employee) obj;
			int age = e.getAge();
			int salary = e.getSalary();
			if(age<20 && salary<20000) {
				throw new IllegalArgumentException("All Details are wrong");
			}else if(age<20) {
				throw new IllegalArgumentException("Age is wrong");
			}else if(salary<20000) {
				throw new IllegalArgumentException("Salary is wrong");
			}
		}
	}
}
