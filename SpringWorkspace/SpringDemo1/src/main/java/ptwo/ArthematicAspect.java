package ptwo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArthematicAspect {

	@Before("execution(* *.*(double,double))")
	public void check1(JoinPoint jpoint) {
		for (Object obj : jpoint.getArgs()) {
			double v =  (Double) obj;
			if(v<0) {
				throw new IllegalArgumentException("Negitve Number");
			}
		}
	}
}
