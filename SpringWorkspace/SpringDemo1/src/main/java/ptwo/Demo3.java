package ptwo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		Arthematic ob;
		ob = (Arthematic) context.getBean("art"); 
		try {
			ob.add(-12, -9894484);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		((AbstractApplicationContext) context).close();
	}
}
