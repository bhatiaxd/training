package pone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		System.out.println("Task 1");
		Hello ob;
		ob = (Hello) context.getBean("Hello");
		ob.display();
		System.out.println("\n\nTask 2");

		Holiday h;
		h= (Holiday) context.getBean("Holiday");
		System.out.println(h);
		
		System.out.println("\n\nTask 3");
		ListOfHolidays loh;
		loh = (ListOfHolidays) context.getBean("loh");
		System.out.println(loh);
		
		
		((ClassPathXmlApplicationContext)context).close();
		
	}
}
