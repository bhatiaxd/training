package pone;


public class Holiday {

		private String holidayDate;
		private String name;
		public Holiday() {
			super();
		}
		public Holiday(String holidayDate, String name) {
			super();
			this.holidayDate = holidayDate;
			this.name = name;
		}
		public String getHolidayDate() {
			return holidayDate;
		}
		public void setHolidayDate(String holidayDate) {
			this.holidayDate = holidayDate;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		@Override
		public String toString() {
		return holidayDate+"\t"+name;
		}
}
