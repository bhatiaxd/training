package pone;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {

	@Bean
	@Scope("singleton")
	public Hello getHello() {
		return new Hello();
	}
	
	
	@Bean
	public Holiday getHoliday() {
		return new Holiday("10/05/1997","Karan");
	}
	
	@Bean
	public ListOfHolidays getLOH() {
		return new ListOfHolidays(Arrays.asList(new Holiday("10/05/1997","Karan"),new Holiday("15/04/1997","Ishbir"),new Holiday("11/11/1997","Snigdha")));
	}
}
