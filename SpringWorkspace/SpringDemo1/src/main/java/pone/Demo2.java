package pone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class Demo2 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
		
		System.out.println("Task 1");
		Hello ob;
		ob =  context.getBean(Hello.class);
		ob.display();
		System.out.println("\n\nTask 2");

		Holiday h;
		h= context.getBean(Holiday.class);
		System.out.println(h);
		
		System.out.println("\n\nTask 3");
		ListOfHolidays loh;
		loh =  context.getBean(ListOfHolidays.class);
		System.out.println(loh);
		
		
		((AbstractApplicationContext) context).close();
		
	}
}
