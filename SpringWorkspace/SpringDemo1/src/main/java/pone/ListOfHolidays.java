package pone;

import java.util.ArrayList;
import java.util.List;

public class ListOfHolidays {
	
	private List<Holiday> loh;

	public ListOfHolidays(List<Holiday> loh) {
		super();
		this.loh = loh;
	}

	public List<Holiday> getLoh() {
		return loh;
	}

	public void setLoh(List<Holiday> loh) {
		this.loh = loh;
	}

	@Override
	public String toString() {
		String s = "";
		for (Holiday holiday : loh) {
			s+=holiday+"\n";
		}
		return 	s;
	}
	public ListOfHolidays() {
		loh = new ArrayList();
	}
	
	

}
