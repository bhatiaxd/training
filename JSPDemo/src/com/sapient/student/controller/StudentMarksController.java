package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.bean.UserBean;
import com.sapient.student.dao.StudentDAO;
import com.sapient.student.exceptions.RollNoNullPointerException;
import com.sapient.student.exceptions.StudentIDNotFound;
import com.sapient.student.others.RBundle;

/**
 * Servlet implementation class StudentMarksController
 */
public class StudentMarksController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentMarksController() {
        super();
        // TODO Auto-generated constructor stub
    }
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	cn= config.getServletContext();


    	super.init(config);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		if(session==null)
		{
			RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
			rs.forward(request, response);
		}
		else
		{
			UserBean usr = (UserBean) session.getAttribute(session.getId());
			if(usr==null)
			{
				session.invalidate();
				RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
				rs.forward(request, response);
			}
			else
			{
				if(usr.isAdmin())
				{
					//Redirect to admin home page
				}
				else
				{
					//Redirect to student home page
					RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
					rs.forward(request, response);
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try {
			String rollno = request.getParameter("rollno");
			if(rollno == null) {
				throw new RollNoNullPointerException(RBundle.getValues("Error1"));
			}
			StudentBean sb = ((Map<String,StudentBean>)cn.getAttribute("smlist")).get(rollno);
			if(sb == null) {
				throw new StudentIDNotFound(RBundle.getValues("Error2"));
			}
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("sdetails", sb);
			rs.forward(request, response);
		}
		catch(StudentIDNotFound s) {
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("msg", s.getMessage());
			rs.forward(request, response);
		}
		catch(RollNoNullPointerException r) {
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("msg", r.getMessage());
			rs.forward(request, response);
		}
		catch(Exception e) {
			
		}
	}
}
