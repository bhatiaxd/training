package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.bean.UserBean;
import com.sapient.student.exceptions.IDorPasswordWrongException;
import com.sapient.student.exceptions.InputFieldEmptyException;
import com.sapient.student.exceptions.RollNoNullPointerException;
import com.sapient.student.exceptions.StudentIDNotFound;
import com.sapient.student.others.RBundle;
import com.sapient.student.bo.*;

/**
 * Servlet implementation class LoginAuth
 */
public class LoginAuth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAuth() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		if(session==null)
		{
			RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
			rs.forward(request, response);
		}
		else
		{
			UserBean usr = (UserBean) session.getAttribute(session.getId());
			if(usr==null)
			{
				session.invalidate();
				RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
				rs.forward(request, response);
			}
			else
			{
				if(usr.isAdmin())
				{
					//Redirect to admin home page
					RequestDispatcher rs = request.getRequestDispatcher("DeleteEdit.jsp");
					rs.forward(request, response);
				}
				else
				{
					//Redirect to student home page
					RequestDispatcher rs = request.getRequestDispatcher("StudentMarksController");
					rs.forward(request, response);
				}
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			String id = request.getParameter("uid");
			String password = request.getParameter("password");
			
			if(id == null || password==null) {
				throw new RollNoNullPointerException(RBundle.getValues("Error1"));
			}
			else if(id.isEmpty() || password.isEmpty())
			{
				throw new InputFieldEmptyException(RBundle.getValues("Error3"));
			}
			Aunthenticate auth = new Aunthenticate();
			String result = auth.auth(id, password);
			
			if(result.equals("A"))
			{
				// Redirect to Admin home page and create session
				HttpSession session = request.getSession(true);
				session.setAttribute(session.getId(), new UserBean(id,password,"A"));
			}
			else if(result.equals("S"))
			{
				HttpSession session = request.getSession(true);
				session.setAttribute(session.getId(), new UserBean(id,password,"S"));
				RequestDispatcher rs = request.getRequestDispatcher("StudentMarksController");
				rs.forward(request, response);
			}
			else
			{				
				throw new IDorPasswordWrongException(RBundle.getValues("Error4"));
			} 
			
		}
		catch(RollNoNullPointerException r) {
			RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
			request.setAttribute("msg", r.getMessage());
			rs.forward(request, response);
		}
		catch (InputFieldEmptyException e) {
			RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
			request.setAttribute("msg", e.getMessage());
			rs.forward(request, response);
		}
		catch (IDorPasswordWrongException e) {
			RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
			request.setAttribute("msg", e.getMessage());
			rs.forward(request, response);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
