package com.sapient.student.bean;

public class UserBean {
	
	private String id;
	private String password;
	private String type;
	
	public UserBean() {
		super();
	}
	public UserBean(String id, String password, String type) {
		super();
		this.id = id;
		this.password = password;
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "UserBean [id=" + id + ", password=" + password + ", type=" + type + "]";
	}
	
	public Boolean isAdmin()
	{
		if(this.type.equals("A"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
