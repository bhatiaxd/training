package com.sapient.student.dao;

import java.sql.*;

public class DBConnection {
static Connection con = null;
	
	public static Connection getConnection() throws Exception {
		try {
			if(con==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@10.151.60.158:1521:xe","hr","hr"); 
			con.setAutoCommit(true);
		} 
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
		
	}

}
