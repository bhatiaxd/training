package com.sapient.week2.try1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class TestUsingMockito {

	static EmployeeDAO edaoMock;
	static List<EmployeeBean> listOfEmployee = new ArrayList<>();

	@BeforeClass
	public static void init() {
		edaoMock = Mockito.mock(EmployeeDAO.class);
		listOfEmployee.addAll(Arrays.asList(new EmployeeBean(1,"Karan",1000),
				new EmployeeBean(2,"Karsan",1000),
				new EmployeeBean(3,"Karan",1000)));
		Mockito.when(edaoMock.readData()).thenReturn(listOfEmployee);
		Mockito.when(edaoMock.getTotSal(Mockito.anyList())).thenCallRealMethod();
		Mockito.when(edaoMock.getCount(Mockito.anyList(),Mockito.anyFloat())).thenCallRealMethod();
	}
	
	
	@Test 
	public void getCount() {
		assertEquals(3,edaoMock.getCount(edaoMock.readData(),1000.0f));
	}

	@Test
	public void totSalary() {
		float sum = edaoMock.getTotSal(edaoMock.readData());
		assertEquals(3000.0f,sum,0.0f);
	}
}
