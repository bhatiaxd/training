package com.sapient.week2.try1;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestEmployee {

	 List<EmployeeBean> listOfEmployee = new ArrayList<>();
	  EmployeeDAO dao = null;
	@Before
	public void init() {
		dao = new EmployeeDAO();
		listOfEmployee.addAll(dao.readData());
		
	}
	
	@Test 
	public void getCount() {
		assertEquals(12,dao.getCount(listOfEmployee,1000.0f));
	}

	@Test
	public void totSalary() {
		assertEquals(12000.0f,dao.getTotSal(listOfEmployee),0.0f);
	}
}
