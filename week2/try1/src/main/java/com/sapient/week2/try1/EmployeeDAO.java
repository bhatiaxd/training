package com.sapient.week2.try1;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO {


		
		
		
		// this method should read the data from csv file and return list.
		public List<EmployeeBean> readData (){
			File dataFile = new File("C:\\Users\\karbhati1\\Desktop\\Test1\\src\\EmployeeData.csv");
			
				List<EmployeeBean> listOfEmployee = new ArrayList<>();
				try(BufferedReader reader = new BufferedReader(new FileReader(dataFile))){
					String line=reader.readLine();
					do{
						listOfEmployee.add(new EmployeeBean(line));
						line=reader.readLine();
					}while(line!= null);
				}
				catch (FileNotFoundException e) {
					System.out.println("Please create a database first");
				} catch (IOException e1) {
					System.out.println("Their was some error with the reader so please try again later");
				} catch (Exception e) {
					System.out.println("Please try again later");
				}
				return listOfEmployee ;
			
		}


		// this method should return total salary of all the employees by lambda expression 
		public float  getTotSal(List<EmployeeBean> emp){
			float sum =0f;
			for (EmployeeBean employeeBean : emp) {
				sum += employeeBean.getSalary(); 
			}
			return sum;
		}

		// this method  should return how many employees are drawing the given salary by lambda expression.
		public int  getCount (List<EmployeeBean> emp, float salary){
			
			return (int) emp.stream().filter(e ->{
				return e.getSalary() == salary;
			}).count();
		}
		
		
		// this method should return the employee details for a given id
		public EmployeeBean getEmpolyee(int id){
			List<EmployeeBean> listOfEmployee = (List<EmployeeBean>) this.readData().stream().filter(e ->{
				return e.getId() == id;
			}).collect(Collectors.toList());
			return  listOfEmployee.get(0);
		}

		//Handle exceptions for file not found, null pointer exception, employee not found user defined exception.

}
