package com.sapient.week2.try1;

public class EmployeeBean {

	private int id;
	private String name;
	private float salary;

	public EmployeeBean(int id, String name, float salary) {
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	public EmployeeBean(String data) {
		String[] fields = data.split(","); 
		this.id = Integer.parseInt(fields[0].trim());
		this.name = fields[1].trim();
		this.salary = Float.parseFloat(fields[2].trim());
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Id:\t" + this.getId() + "\nName:\t" + this.getName() + "\nSalary:\t" + this.getSalary();
	}

	@Override
	public boolean equals(Object obj) {
		EmployeeBean emp = (EmployeeBean) obj;
		return this.getId() == emp.getId();
	}

}
