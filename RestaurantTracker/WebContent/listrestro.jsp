<%@page import="java.io.StringReader"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.Reader"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="javax.json.JsonObject"%>
<%@page import="java.net.URL"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="javax.json.Json"%>
<%@page import="javax.ws.rs.core.MediaType"%>
<%@page import="javax.ws.rs.core.UriBuilder"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.WebTarget"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page import="org.glassfish.jersey.client.ClientConfig"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List</title>
</head>
<%
 String userKey = "0d9f75489caced39b9ba2fe0753dceb5";
ClientConfig configs = new ClientConfig();  
Client client = ClientBuilder.newClient(configs); 
URL url = new URL("https://developers.zomato.com/api/v2.1/locations?query=Ashok%20vihar&count=1");
WebTarget target = client.target(UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/").build());  

String s = target.path("establishments").queryParam("city_id", "1").request().header("user-key", userKey).accept(MediaType.APPLICATION_JSON).get(String.class);
%>
<body>
<p><%=s %>
</p>

</body>
</html>