package com.sapient.restaurant.dao;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.restaurant.bean.LongLat;

public class LonLatDAO {

	public static LongLat setLL(String city) {
		LongLat l = new LongLat("0","0");
		String userKey = "0d9f75489caced39b9ba2fe0753dceb5";
		ClientConfig configs = new ClientConfig();  
		Client client = ClientBuilder.newClient(configs); 
		WebTarget target = client.target(UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/").build());  

		String s = target.path("locations").queryParam("query", city).request().header("user-key", userKey).accept(MediaType.APPLICATION_JSON).get(String.class);
		 JsonReader rdr = Json.createReader(new StringReader(s));
		 System.out.println(s);
		    JsonObject obj = rdr.readObject();
		     JsonArray results = obj.getJsonArray("location_suggestions");

		     for (JsonObject result : results.getValuesAs(JsonObject.class)) {
		    	 		l.setLattitude(result.get("latitude").toString());
		    	 		l.setLongitude(result.get("longitude").toString());
		    	     }
		
		System.out.println(l.getLattitude()+""+l.getLongitude());
		return l;
		
	}
	public static void main(String[] args) {
		setLL("Ashok Vihar");
	}
}
