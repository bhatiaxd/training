package com.sapient.restaurant.bo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.restaurant.bean.LongLat;
import com.sapient.restaurant.dao.LonLatDAO;

/**
 * Servlet implementation class NearMe
 */
public class NearMe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NearMe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userKey = "0d9f75489caced39b9ba2fe0753dceb5";
		String city =  request.getParameter("city");
		System.out.println(city);
		LongLat l = LonLatDAO.setLL(city);
		ClientConfig configs = new ClientConfig();  
		Client client = ClientBuilder.newClient(configs); 
		WebTarget target = client.target(UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/").build());  

		String res = target.path("geocode").queryParam("lat", l.getLattitude()).queryParam("lon", l.getLongitude()).request().header("user-key", userKey).accept(MediaType.APPLICATION_JSON).get(String.class);
		System.out.println(res);
		RequestDispatcher rd = request.getRequestDispatcher("nearme.jsp");
		request.setAttribute("listnearme", res);
		request.setAttribute("city",city);
		rd.forward(request, response);
	}

}
