package com.sapient.restaurant.bean;

public class Location {

	
		public String entity_type ;//(string, optional): Type of location; one of [city, zone, subzone, landmark, group, metro, street] ,
		public String entity_id ;//(integer, optional): ID of location; (entity_id, entity_type) tuple uniquely identifies a location ,
		public String title ;//(string, optional): Name of the location ,
		public String latitude;// (number, optional): Coordinates of the (centre of) location ,
		public String longitude;// (number, optional): Coordinates of the (centre of) location ,
		public String city_id ;//(integer, optional): ID of city ,
		public String city_name;// (string, optional): Name of the city ,
		public String country_id ;//(integer, optional): ID of country ,
		public String country_name;// (string, optional): Name of the country 
		
}
