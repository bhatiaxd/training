/**
 * 
 */
package com.sapient.restaurant.bean;

/**
 * @author karbhati1
 *
 */
public class LongLat {

	private String longitude;
	private String lattitude;
	public LongLat(String longitude, String lattitude) {
		super();
		this.longitude = longitude;
		this.lattitude = lattitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	
	
}
