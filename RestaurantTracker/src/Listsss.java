import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class Listsss {
public static void main(String[] args) throws Exception {
	String userKey = "0d9f75489caced39b9ba2fe0753dceb5";
	ClientConfig configs = new ClientConfig();  
	Client client = ClientBuilder.newClient(configs); 
	WebTarget target = client.target(UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/").build());  

	String s = target.path("establishments").queryParam("city_id", "1").request().header("user-key", userKey).accept(MediaType.APPLICATION_JSON).get(String.class);
	 JsonReader rdr = Json.createReader(new StringReader(s));

	    JsonObject obj = rdr.readObject();
	     JsonArray results = obj.getJsonArray("establishments");

	     for (JsonObject result : results.getValuesAs(JsonObject.class)) {
	    	 		System.out.print(result.getJsonObject("establishment").get("id"));
	    	 			
	    	          System.out.print(": ");
	    	         System.out.println(result.getJsonObject("establishment").get("name"));
	    	     }
}
}
